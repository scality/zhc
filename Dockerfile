

FROM docker.io/ubuntu:18.04
  
ADD zendesk-helpcenter-cms /zendesk-helpcenter-cms
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install --no-install-recommends -y \
        git \
        python3-buildbot-worker \
        python3-docutils \
        python3-setuptools \
        python3-pip \
        make \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    cd /zendesk-helpcenter-cms && \
    python3 setup.py install

CMD zendesk-help-cms -r $PATH_ARTICLES export
