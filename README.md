# Zendesk Help Center - Scality fork

This is the fork of zendesk-helpcenter-cms, with heavy code
additions:

* Text files are RST formatted,
* The script is stateless: it doesn't write local, hidden metadata files anymore,
* One can integrate images or downloadable files, the script will upload them as attachments on zendesk,
* The "export" command can be given a single .rst file to be exported.

For now this script is used as an automatic publishing tool for the [TSKB](https://scality.atlassian.net/secure/RapidBoard.jspa?projectKey=TSKB&useStoredSettings=true&rapidView=137) project. Eve launches it after the merges on the [tskb git repository](https://bitbucket.org/scality/tskb).
