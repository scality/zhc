import os
import utils
import markdown
import scality_rst2html
import re

DEFAULT_LOCALE = 'en-us'


class Base(object):
    _meta_exp = '.meta'
    _content_exp = '.json'
    _zendesk_id_key = 'id'

    def __init__(self, name, filename):
        super().__init__()
        self.name = name
        self.filename = filename
        self.translations = []
        self._meta = {}

    @property
    def meta(self):
        return self._meta

    @meta.setter
    def meta(self, value):
        self._meta = value or {}

    @property
    def zendesk_id(self):
        return self._meta.get(self._zendesk_id_key)

    @property
    def meta_filepath(self):
        return os.path.join(self.path, self.meta_filename + self._meta_exp)

    @property
    def content_filepath(self):
        return os.path.join(self.path, self.content_filename + self._content_exp)


class GroupTranslation(object):

    def __init__(self, locale, name, description,group):
        self.locale = locale or DEFAULT_LOCALE
        self.name = name
        self.description = description
        self.group = group

    def to_dict(self, image_cdn=None):
        return {
            'title': self.name,
            'body': self.description,
            'locale': utils.to_zendesk_locale(self.locale),
            'group': self.group
        }


class Group(Base):
    meta_filename = '.group'
    content_filename = '__group__'

    def __init__(self, name, description, filename):
        super().__init__(name, filename)
        self.description = description

    def to_content(self):
        return {
            'name': self.name,
            'description': self.description
        }

    def to_dict(self, image_cdn=None):
        return {
            'name': self.name,
            'description': self.description,
            'locale': utils.to_zendesk_locale(DEFAULT_LOCALE)
        }

    def content_translation_filepath(self, locale):
        if locale:
            locale = '.' + locale
        return os.path.join(self.path, self.content_filename + locale + self._content_exp)

    def paths(self):
        return [self.content_filepath]


class Category(Group):
    zendesk_name = 'category'
    zendesk_group = 'categories'

    def __init__(self, name, description, filename):
        super().__init__(name, description, filename)
        self.sections = []

    @property
    def path(self):
        return self.filename

    @staticmethod
    def from_dict(meta, content, filename):
        name = content['name']
        description = content.get('description', '')
        category = Category(name, description, filename)
        category.meta = meta
        category.group = content['group'] if 'group' in content else False
        return category

    @classmethod
    def filepaths_from_path(cls, path):
        meta_path = os.path.join(path, cls.meta_filename + cls._meta_exp)
        content_path = os.path.join(path, cls.content_filename + cls._content_exp)
        return meta_path, content_path

    @property
    def new_item_url(self):
        return 'categories.json'


class Section(Group):
    zendesk_name = 'section'
    zendesk_group = 'sections'

    def __init__(self, category, name, description, filename):
        super().__init__(name, description, filename)
        self.articles = []
        self.category = category

    @property
    def path(self):
        return os.path.join(self.category.path, self.filename)

    @classmethod
    def filepaths_from_path(cls, category, path):
        meta_path = os.path.join(category.path, path, cls.meta_filename + cls._meta_exp)
        content_path = os.path.join(category.path, path, cls.content_filename + cls._content_exp)
        return meta_path, content_path

    @staticmethod
    def from_dict(category, meta, content, filename):
        name = content['name']
        description = content.get('description', '')
        section = Section(category, name, description, filename)
        section.meta = meta
        section.group = content['group'] if 'group' in content else False
        return section

    @property
    def new_item_url(self):
        return 'categories/{}/sections.json'.format(self.category.zendesk_id)

class ArticleTranslation(object):
    def __init__(self, locale, name, body, group):
        self.locale = locale
        self.name = name
        self.body = body
        self.group = group

    def to_dict(self, image_cdn=None):
        raw = self.body
        body=""
        images={}
        p = re.compile('`.+\s+<(.+)>`_')
        for line in raw.split('\n'):
            # Search for images
            if 'image::' in line:
                words=line.lstrip().split(' ')
                images[words[(len(words)-1)]]=""
            # Search for link to attachment
            m = p.search(line)
            if m:
                images[m.group(1)]=""
            body+="{}\n".format(line)
        body = scality_rst2html.rst2html(body)

        return {
            'title': self.name,
            'body': body,
            'locale': utils.to_zendesk_locale(self.locale),
            'group': self.group,
            'images': images
        }

class Article(Base):
    zendesk_name = 'article'
    zendesk_group = 'articles'

    _body_exp = '.rst'
    _meta_pattern = '.article_{}'

    def __init__(self, section, name, body, filename):
        super().__init__(name, filename)
        self.body = body
        self.section = section

    @property
    def meta_filename(self):
        return self._meta_pattern.format(self.content_filename)

    @property
    def content_filename(self):
        return self.filename

    @property
    def body_filepath(self):
        return os.path.join(self.path, self.content_filename + self._body_exp)

    @property
    def path(self):
        return self.path_from_section(self.section)

    def to_dict(self, image_cdn=None):
        raw = self.body
        body=""
        images={}
        p = re.compile('`.+\s+<(.+)>`_')
        for line in raw.split('\n'):
            # Search for images
            if 'image::' in line:
                words=line.lstrip().split(' ')
                images[words[(len(words)-1)]]=""
            # Search for link to attachment
            m = p.search(line)
            if m:
                images[m.group(1)]=""
            body+="{}\n".format(line)
        body = scality_rst2html.rst2html(body)

        return {
            'title': self.name,
            'body': body,
            'images': images
        }
    def to_content(self):
        return {
            'name': self.name
        }

    def content_translation_filepath(self, locale):
        return os.path.join(self.path, locale, self.content_filename + self._content_exp)

    def body_translation_filepath(self, locale):
        return os.path.join(self.path, locale, self.content_filename + self._body_exp)

    def paths(self):
        return [self.content_filepath, self.body_filepath]

    @staticmethod
    def path_from_section(section):
        return section.path

    @classmethod
    def filepaths_from_path(cls, section, name):
        path = cls.path_from_section(section)
        meta_path = os.path.join(path, cls._meta_pattern.format(name) + cls._meta_exp)
        content_path = os.path.join(path, name + cls._content_exp)
        body_path = os.path.join(path, name + cls._body_exp)
        return meta_path, content_path, body_path

    @staticmethod
    def from_dict(section, meta, content, body, filename):
        article = Article(section, content['name'], body, filename)
        article.meta = meta
        return article

    @property
    def new_item_url(self):
        return 'sections/{}/articles.json'.format(self.section.zendesk_id)
