import logging
import requests
import json
import html2text
import hashlib
import html
from operator import attrgetter
import os
import model
import utils

requests.packages.urllib3.disable_warnings()

class ZendeskRequest(object):
    _default_url = 'https://{}/api/v2/help_center/' + utils.to_zendesk_locale(model.DEFAULT_LOCALE) + '/{}'
    _translations_url = 'https://{}/api/v2/help_center/{}'
    _user_segments_url = 'https://{}/api/v2/help_center/user_segments/'
    _permission_groups_url = 'https://{}/api/v2/guide/permission_groups.json'
    _attachments_url = 'https://{}/api/v2/help_center/articles/{}/attachments.json'
    _articles_url = 'https://{}/api/v2/help_center/sections/{}/articles.json'
    _sections_url = 'https://{}/api/v2/help_center/categories/{}/sections.json'
    _categories_url = 'https://{}/api/v2/help_center/categories.json'
    _attachment_url  ='https://{}/api/v2/help_center/articles/attachments/{}.json'

    item_url = '{}/{}.json'
    items_url = '{}.json?per_page=100'
    items_in_group_url = '{}/{}/{}.json?per_page=100'

    translation_url = '{}/{}/translations/{}.json'
    translations_url = '{}/{}/translations.json?per_page=100'
    missing_translations_url = '{}/{}/translations/missing.json'

    user_segments_id = {}
    permission_group_id = {}
    category_ids = {}
    section_ids = {}
    article_ids = {}
    attachment_ids = {}

    def __init__(self, company_uri, user, password):
        super().__init__()
        self.company_uri = company_uri
        self.user = user
        self.password = password

    def _url_for(self, path):
        return self._default_url.format(self.company_uri, path)

    def _translation_url_for(self, path):
        return self._translations_url.format(self.company_uri, path)

    def _parse_response(self, response):
        if response.status_code == 404:
            raise RecordNotFoundError('Missing record for {}'.format(response.url))
        if response.status_code not in [200, 201]:
            logging.error('getting data from %s failed. status was %s and message %s',
                          response.url, response.status_code, response.text)
            return {}
        return response.json()

    def _send_request(self, request_fn, url, data):
        full_url = self._url_for(url)
        response = request_fn(full_url, data=json.dumps(data),
                              auth=(self.user, self.password),
                              headers={'Content-type': 'application/json'},
                              verify=False)
        return self._parse_response(response)

    def _send_translation(self, request_fn, url, data):
        full_url = self._translation_url_for(url)
        response = request_fn(full_url, data=json.dumps(data),
                              auth=(self.user, self.password),
                              headers={'Content-type': 'application/json'},
                              verify=False)
        return self._parse_response(response)

    def get_item(self, item):
        url = self.item_url.format(item.zendesk_group, item.zendesk_id)
        full_url = self._url_for(url)
        response = requests.get(full_url, auth=(self.user, self.password), verify=False)
        return self._parse_response(response).get(item.zendesk_name, {})

    def get_items(self, item, parent=None):
        if parent:
            url = self.items_in_group_url.format(parent.zendesk_group, parent.zendesk_id, item.zendesk_group)
        else:
            url = self.items_url.format(item.zendesk_group)
        full_url = self._url_for(url)
        response = requests.get(full_url, auth=(self.user, self.password), verify=False)
        return self._parse_response(response).get(item.zendesk_group, {})

    def get_missing_locales(self, item):
        url = self.missing_translations_url.format(item.zendesk_group, item.zendesk_id)
        full_url = self._translation_url_for(url)
        response = requests.get(full_url, auth=(self.user, self.password), verify=False)
        return self._parse_response(response).get('locales', [])

    def get_translation(self, item, locale):
        if locale == '':
            locale = utils.to_zendesk_locale(model.DEFAULT_LOCALE)
        url = self.translation_url.format(item.zendesk_group, item.zendesk_id, locale)
        full_url = self._translation_url_for(url)
        response = requests.get(full_url, auth=(self.user, self.password), verify=False)
        return self._parse_response(response).get('translation', {})

    def put(self, item, data):
        url = self.item_url.format(item.zendesk_group, item.zendesk_id)
        return self._send_request(requests.put, url, data).get(item.zendesk_name, {})

    def put_translation(self, item, locale, data):
        if locale == '':
            locale = utils.to_zendesk_locale(model.DEFAULT_LOCALE)
        url = self.translation_url.format(item.zendesk_group, item.zendesk_id, locale)
        return self._send_translation(requests.put, url, data).get('translation', {})

    def post(self, item, data, parent=None):
        if parent:
            url = self.items_in_group_url.format(parent.zendesk_group, parent.zendesk_id, item.zendesk_group)
        else:
            url = self.items_url.format(item.zendesk_group)
        return self._send_request(requests.post, url, data).get(item.zendesk_name, {})

    def post_translation(self, item, data):
        url = self.translations_url.format(item.zendesk_group, item.zendesk_id)
        return self._send_translation(requests.post, url, data).get('translation', {})

    def post_image(self,zendesk_id, binary):
        url = self._attachments_url.format(self.company_uri,zendesk_id)
        return requests.post(url,files={"inline":"true","file":binary},auth=(self.user, self.password))

    def delete(self, item):
        url = self.item_url.format(item.zendesk_group, item.zendesk_id)
        full_url = self._url_for(url)
        return self.raw_delete(full_url)

    def raw_delete(self, full_url):
        response = requests.delete(full_url, auth=(self.user, self.password), verify=False)
        return response.status_code == 200

class RecordNotFoundError(Exception):
        pass

