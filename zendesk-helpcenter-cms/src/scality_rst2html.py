from docutils.core import publish_parts

def rst2html(s):
    return publish_parts(s, writer_name='html5')['html_body'].encode('utf-8').decode('utf-8')
