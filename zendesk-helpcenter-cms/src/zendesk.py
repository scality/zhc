import logging
import requests
import json
import html2text
import hashlib
import html
from operator import attrgetter
import os
import model
import utils
from zendeskrequest import ZendeskRequest

class Fetcher(object):

    def __init__(self, req):
        super().__init__()
        self.req = req

    def fetch(self):
        categories = []
        zendesk_categories = self.req.get_items(model.Category)
        for zendesk_category in zendesk_categories:
            category_filename = utils.slugify(zendesk_category['name'])
            category = model.Category(zendesk_category['name'], zendesk_category['description'], category_filename)
            print('Category %s created' % category.name)
            category.meta = zendesk_category
            zendesk_sections = self.req.get_items(model.Section, category)
            categories.append(category)
            for zendesk_section in zendesk_sections:
                section_filename = utils.slugify(zendesk_section['name'])
                section = model.Section(category, zendesk_section['name'],
                                        zendesk_section['description'], section_filename)
                print('Section %s created' % section.name)
                section.meta = zendesk_section
                zendesk_articles = self.req.get_items(model.Article, section)
                category.sections.append(section)
                for zendesk_article in zendesk_articles:
                    body = html2text.html2text(zendesk_article.get('body', ''))
                    article_filename = utils.slugify(zendesk_article['title'])
                    article = model.Article(section, zendesk_article['title'], body, article_filename)
                    print('Article %s created' % article.name)
                    article.meta = zendesk_article
                    section.articles.append(article)
        return categories


class Pusher(object):

    def __init__(self, req, fs, image_cdn, disable_comments):
        self.req = req
        self.fs = fs
        self.image_cdn = image_cdn
        self.disable_comments = disable_comments
        self.dict_image = {}

    def _has_content_changed(self, translation, item, locale):
        zendesk_content = self.req.get_translation(item, locale)
        item_content = translation.to_dict(self.image_cdn)
        if item.zendesk_group == 'articles' : 
            item_content=self._push_images(item,item_content)
        ignore_keys=['locale','group','images']
        for key in item_content:
            if key == 'body' and (item.zendesk_group == 'categories' or item.zendesk_group == 'sections') : continue
            if key in ignore_keys : continue
            replace = {
                '><':'>\n<',
                ' />':'>',
                '&gt;':'>',
                '&lt;':'<',
                '&amp;':'&',
                '>></':'>>\n</'
            }
            zendesk_body = zendesk_content.get(key, '')
            item_unescaped=html.unescape(item_content[key])

            for pattern in replace:
                zendesk_body=zendesk_body.replace(pattern,replace[pattern])
                item_unescaped=item_unescaped.replace(pattern,replace[pattern])
            zendesk_hash = hashlib.md5(zendesk_body.encode('utf-8'))
            item_hash = hashlib.md5(item_unescaped.encode('utf-8'))
            if zendesk_hash.hexdigest() != item_hash.hexdigest():
                return True
        return False

    def _push_new_item(self, item, parent=None, section_group=None, category_group=None):
        data = {item.zendesk_name: item.to_dict(self.image_cdn)}
        if category_group != None or section_group != None:
            self._set_permissions(data[item.zendesk_name], section_group, category_group)
        item_content = item.to_dict(self.image_cdn)
        meta = self.req.post(item, data, parent)
        item.meta = meta
        if item.zendesk_group == 'articles':
            item_content=self._push_images(item,item_content)

    def _push_item_translations(self, item, section_group, category_group):
        missing_locales = self.req.get_missing_locales(item)
        for translation in item.translations:
            locale = utils.to_zendesk_locale(translation.locale)
            data = {'translation': translation.to_dict(self.image_cdn)}
            if locale in missing_locales:
                print('{} "{}" has new translation for locale {}'.format(item.zendesk_name,item.name,translation.locale))
                if item.zendesk_group == 'articles' : 
                    data['translation']=self._push_images(item,data['translation'])
                self.req.post_translation(item, data)
            else:
                if self._has_content_changed(translation, item, locale):
                    print('Change detected for locale {} of {} "{}", updating...'.format(locale,item.zendesk_name,item.name))
                    if item.zendesk_id in self.dict_image:
                        data = {'translation': self.dict_image[item.zendesk_id]}
                    self.req.put_translation(item, locale, data)
                else:
                    print('Nothing changed for locale {} of {} "{}", go for next...'.format(locale,item.zendesk_name,item.name))

    def _disable_article_comments(self, article):
        data = {
            'comments_disabled': True
        }
        self.req.put(article, data)

    def _get_user_segment_id(self,group):
        if group in self.req.user_segments_id : return self.req.user_segments_id[group]
        full_url=self.req._user_segments_url.format(self.req.company_uri)
        response = requests.get(full_url, auth=(self.req.user, self.req.password), verify=False)
        user_segments=self.req._parse_response(response)['user_segments']
        for user_segment in user_segments:
            if user_segment['name'] == group:
                self.req.user_segments_id[group] = user_segment['id']
                return user_segment['id']
        print('User segment {} does not exists in zendesk. Leaving ...'.format(group))
        exit()

    def _get_permission_group_id(self,group):
        if group in self.req.permission_group_id : return self.req.permission_group_id[group]
        full_url=self.req._permission_groups_url.format(self.req.company_uri)
        response = requests.get(full_url, auth=(self.req.user, self.req.password), verify=False)
        perm_groups=self.req._parse_response(response)['permission_groups']
        for perm_group in perm_groups:
            if perm_group['name'] == group:
                self.req.permission_group_id[group] = perm_group['id']
                return perm_group['id']
        print('Permission Group {} does not exists in zendesk. Leaving ...'.format(group))
        exit()

    def _set_permissions(self, article, local_data,section_group,category_group):
        current_group = self.req.get_item(article)['user_segment_id']
        if local_data['group'] != False: group=local_data['group']
        elif section_group != False: group = section_group
        elif category_group != False: group = category_group
        if 'group' in vars():
            print('Article {} will be visible from {}'.format(local_data['title'],group))
            usid = self._get_user_segment_id(group)        
            if current_group != usid: self.req.put(article,{'user_segment_id':usid})
        else: 
            print('/!\\ Article {} will be visible from everyone'.format(local_data['title']))
            if current_group != None: self.req.put(article,{'user_segment_id':None})

    def _set_permissions(self, article,section_group,category_group):
        # Read permissions
        if section_group != False: group = section_group
        elif category_group != False: group = category_group
        if 'group' in vars():
            print('Article will be visible from {}'.format(group))
            usid = self._get_user_segment_id(group)        
            article['user_segment_id']=usid
        else: 
            print('/!\\ Article will be visible from everyone')
            article['user_segment_id']=None
        # Write permissions
        article['permission_group_id']=self._get_permission_group_id('Agents and Managers')

    def _attachment_url(self,name,article_id):
        if article_id in self.req.attachment_ids.keys():
            if name in self.req.attachment_ids[article_id]:
                return self.req.attachment_ids[article_id][name]
        full_url=self.req._attachments_url.format(self.req.company_uri,article_id)
        attachments=self.req._parse_response(requests.get(full_url,auth=(self.req.user, self.req.password), verify=False))['article_attachments']
        for att in attachments:
            # Cache article <-> attachment
            if article_id not in self.req.attachment_ids.keys():
                self.req.attachment_ids[article_id]={}
            self.req.attachment_ids[article_id][att['file_name']] = att['content_url']
            if att['file_name'] == name:
                return att['content_url']
        print('No attachment file called "{}" in article ID {}  on zendesk site'.format(name,article_id))
        return None

    def _compare_md5Checksum(self,filePath,url):
        with open(filePath, 'rb') as fh:
            local_m = hashlib.md5()
            while True:
                data = fh.read(8192)
                if not data:
                    break
                local_m.update(data)
            local_sum = local_m.hexdigest()
        remote_m = hashlib.md5()
        r = requests.get(url,auth=(self.req.user, self.req.password), verify=False)
        for data in r.iter_content(8192):
             remote_m.update(data)
        remote_sum  = remote_m.hexdigest()
        return remote_sum == local_sum

    def _push_images(self,item,article):
        if (article['images']) == 0: return article
        for image in article['images']:
            if not os.path.isfile(image):
                print('File {} not found'.format(image))
                continue
            # Compare local and remote image checksum
            image_url=self._attachment_url(os.path.basename(image),item.zendesk_id)
            push_image=True
            if image_url:
                if self._compare_md5Checksum(image,image_url):
                    push_image = False
                else:
                    # Local and remote images are different. Need to delete
                    # the remote one.
                    # Extract the attachment ID
                    old_id=image_url.split('/')[5]
                    print('Deleting old attachment ID {}'.format(old_id))
                    self.req.raw_delete(self.req._attachment_url.format(self.req.company_uri,old_id))
	
            if push_image:
                binary=open(image,'rb')
                image_meta=json.loads(self.req.post_image(item.zendesk_id,binary).text)
                image_url=image_meta['article_attachment']['content_url']
            # Now that the image/attachment is on ZD site, modify the article
            # To point to the correct URL
            article['body']=article['body'].replace(image,image_url)
            self.dict_image[item.zendesk_id] = article
        return article

    def _push(self, item, parent=None, section=None, category=None):
        if not item.zendesk_id:
            print('Pushing new item of type {} called "{}"'.format(item.zendesk_name,item.name))
            self._push_new_item(item, parent, section.group if section else None, category.group if category else None)
        self._push_item_translations(item, section.group if section else None, category.group if category else None)

    def push(self, categories):
        print("Full export not supported. export a single .rst file.")
        exit()
        for category in categories:
            if (category.name == 'images'): continue
            print('Pushing category %s' % category.name)
            self._push(category)
            for section in category.sections:
                print('Pushing section %s' % section.name)
                self._push(section, category)
                for article in section.articles:
                    print('Pushing article %s' % article.name)
                    self._push(article, section, section.group, category.group)
                    if self.disable_comments:
                        self._disable_article_comments(article)

    def push_article(self, article):
        section = article.section
        category = section.category
        self._push(category)
        self._push(section, category)
        self._push(article, section, section, category)
        if self.disable_comments:
            self._disable_article_comments(article)


class Remover(object):

    def __init__(self, req):
        self.req = req

    def remove(self, item):
        if item.zendesk_id:
            self.req.delete(item)


class Mover(object):

    def __init__(self, req, image_cdn):
        self.req = req
        self.image_cdn = image_cdn

    def move(self, item):
        self.req.put(item)


class Doctor(object):

    def __init__(self, req, fs, force=False):
        self.req = req
        self.fs = fs
        self.force = force

    def _merge_items(self, zendesk_items):
        if self.force:
            print('There are {} entries with the same name {}, this should be an error. Since the command was run '
                  'with --force option enabled every entry except the oldest will be removed'.format(
                      len(zendesk_items), zendesk_items[0]['name']))
            sorted_items = sorted(zendesk_items, key=attrgetter('updated_at'))
            for item in sorted_items[:-1]:
                print('removing item with id: {}'.format(item['id']))
                self.req.raw_delete(item['url'])
            return sorted_items[0]
        else:
            print('There are {} entries with the same name {}:'.format(len(zendesk_items), zendesk_items[0]['name']))
            for idx, item in enumerate(zendesk_items):
                print('{}. created: {}, updated: {}, link: {}'.format(idx + 1, item['created_at'], item['updated_at'], item['html_url']))
            article_nr = int(input('Pick a number you wish to keep or 0 to keep all of them: '))

            if article_nr == 0 or article_nr > len(zendesk_items) + 1:
                return zendesk_items[0]

            for idx, item in enumerate(zendesk_items):
                if not article_nr == idx + 1:
                    print('removing item with id: {}'.format(item['id']))
                    self.req.raw_delete(item['url'])
            return zendesk_items[article_nr - 1]

    def _fetch_item(self, item, parent=None):
        zendesk_items = self.req.get_items(item, parent)
        named_items = list(filter(lambda i: i['name'] == item.name, zendesk_items))
        if len(named_items) > 1:
            return self._merge_items(named_items)
        if len(named_items) == 1:
            return named_items[0]
        return {}

    def _exists(self, item):
        try:
            self.req.get_item(item)
        except RecordNotFoundError:
            return False
        return True

    def _fix_item(self, item, parent=None):
        if parent and not parent.zendesk_id:
            if item.zendesk_id:
                logging.warning('Parent is a new item but Zendesk ID exists. Removing meta...')
            self.fs.remove(item.meta_filepath)
            item.meta = {}
            return

        try:
            zendesk_item = self._fetch_item(item, parent)
            if item.zendesk_id:
                if zendesk_item and zendesk_item.get('id') != item.zendesk_id:
                    print('Zendesk ID is incorrect but found item with the same name {}.'
                          ' If this is not corrent you need to fix it manually'.format(item.name))
                    item.meta = zendesk_item
                    self.fs.save_json(item.meta_filepath, zendesk_item)
                elif not zendesk_item:
                    print('Zendesk ID is incorrect and no item with the same name'
                          ' was found for name {}. Assuming new item'.format(item.name))
                    self.fs.remove(item.meta_filepath)
                    item.meta = {}
            else:
                if zendesk_item:
                    print('Zendesk ID is missing but found item with the same name {}.'
                          ' If this is not correct you need to fix it manually'.format(item.name))
                    item.meta = zendesk_item
                    self.fs.save_json(item.meta_filepath, zendesk_item)

        except RecordNotFoundError as e:
            logging.warning(str(e))

    def fix(self, categories):
        for category in categories:
            self._fix_item(category)
            for section in category.sections:
                self._fix_item(section, section.category)
                for article in section.articles:
                    self._fix_item(article, article.section)


class RecordNotFoundError(Exception):
    pass


def fetcher(company_uri, user, password):
    req = ZendeskRequest(company_uri, user, password)
    return Fetcher(req)


def pusher(company_uri, user, password, fs, image_cdn, disable_comments):
    req = ZendeskRequest(company_uri, user, password)
    return Pusher(req, fs, image_cdn, disable_comments)


def remover(company_uri, user, password):
    req = ZendeskRequest(company_uri, user, password)
    return Remover(req)


def mover(company_uri, user, password, image_cdn):
    req = ZendeskRequest(company_uri, user, password)
    return Mover(req, image_cdn)


def doctor(company_uri, user, password, fs, force):
    req = ZendeskRequest(company_uri, user, password)
    return Doctor(req, fs, force)
