#!/bin/bash

HOST_PATH_ARTICLES=/Users/boris/Desktop/tskb/articles
CONTAINER_PATH_ARTICLES=/articles

docker build -t zhc  .
docker run -d --name zhc --mount type=bind,source=${HOST_PATH_ARTICLES},target=/articles -e PATH_ARTICLES=${CONTAINER_PATH_ARTICLES} zhc
